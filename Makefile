IMAGE   := "dleske/tester.hugo"
SOURCES := Dockerfile

all: .release

.release: $(SOURCES)
	docker buildx build \
		--platform linux/arm64/v8,linux/amd64 \
		--push \
		--tag $(IMAGE) .
	touch .release
