# tester.hugo test container for Hugo

This builds a container image that can be used for testing Hugo-generated
static websites.  It can build the given website, and can check it for bad
links using the [linkchecker](https://pypi.org/project/linkchecker) program.
