FROM alpine:latest
#
# For checking Hugo-generated websites.
# Test with: docker run -it -v $PWD/website:/website -w /website tester.hugo /bin/sh
# Generate site with `hugo --config=hugo.toml`
# Then run linkchecker: `linkchecker public/`
#
RUN apk add hugo python3 py3-pip tzdata git make rsync openssh
RUN pip install linkchecker
